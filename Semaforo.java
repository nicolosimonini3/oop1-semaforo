package OOP1_Semaforo;

class Semaforo {

    public static final String RED = "\033[0;31m O";
    public static final String YELLOW = "\033[0;33m O";
    public static final String GREEN = "\033[0;32m O";
    public static final String RED_BRIGHT = "\033[1;91m O";
    public static final String YELLOW_BRIGHT = "\033[1;93m O";
    public static final String GREEN_BRIGHT = "\033[1;92m O";

    String color = RED_BRIGHT;

    int timer = 500000, current= 0, count=0;

    public Semaforo(int tick) {
        timer = tick;
    }

    public int timer() {
        while (true) {
            if (current < 0) {
                count++;
                print_stoplight();
                change_color();
                current = timer;

                if(count%3==0) System.out.println();
            }
            current--;
        }
    }

    public void change_color(){
        switch (color){
            case RED_BRIGHT: color = GREEN_BRIGHT;
            break;
            case YELLOW_BRIGHT: color = RED_BRIGHT;
            break;
            case GREEN_BRIGHT: color = YELLOW_BRIGHT;
            break;
        }
    }

    public void clear(){
        for (int i = 0; i < 5; i++) {
            System.out.println();
        }
    }

    public void print_stoplight(){
        clear();
        switch (color){
            case RED_BRIGHT: System.out.println(RED_BRIGHT+"\n"+YELLOW+"\n"+GREEN+"\n");
            break;
            case YELLOW_BRIGHT: System.out.println(RED+"\n"+YELLOW_BRIGHT+"\n"+GREEN+"\n");
            break;
            case GREEN_BRIGHT: System.out.println(RED+"\n"+YELLOW+"\n"+GREEN_BRIGHT+"\n");
            break;
        }
    }


}